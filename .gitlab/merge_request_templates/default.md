## Merge Request  Summary
Comes here (Describe what the MR is about, use simple words)

Jira: [Issue title](link-of-ticket).


## Checklist before requesting a review

#### Version Control section 💻
- [ ] I have performed a self-review of my code
- [ ] Was the **branch named** with the Jira task? 
- [ ] Was the code **rebase**?
- [ ] Aren't **there more than one Jira Ticket** included in this MR? if not, let's consider splitting the MR into two.
#### Code Standards section 🎓
- [ ] Does the code adhere to **naming conventions**? such us Class and Object Names : Use noun phrase names, Method Names : Use verb phrase names. 
- [ ] Do the functions  do only thing ?. Adhering to the **Single Responsibility principle**. 
- [ ] Does the code adhere to the **DRY** principle?
- [ ] Does the code keep **Separation of Concerns** ( Example create locators in the expected Web Page Class )?
- [ ] Isn't there any  **hardcode** data? 
- [ ] Does the code adhere to **KISS** (Keep it simple) - Simplicity (and avoiding complexity) should always be a key goal
- [ ] Was the code written for the Maintainer -  keep in mind someone with little knowledge of the system will come after you to change the code.
- [ ] are TODO comments added to mention future improvements properly? instead of general observations.

#### UI Automation section  🌐
- [ ] Are the **locators** reliable? 
- [ ] Don't we have any **hard wait**?
- [ ] Is the code using `@Step` where needed?
- [ ] Is the **test independent**?



## Report portal execution 📊
Comes here 
(Every time you run on your local, it generates the report and can be found on the logs after running the tests locally.)


### ***Thanks for reviewing and providing valuable feedback***.🚀


## References
[Conventions](https://amwell.atlassian.net/wiki/spaces/QA/pages/3364979103/Code+review+template)

